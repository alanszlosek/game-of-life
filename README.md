Welcome
----

This is a simulator for [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life) written in python. It runs as a screensaver of sorts for my in-office linux server (5Ghz wifi access point, raid1 NAS, webapps, etc) perched high on a bookshelf behind me.

If you want to know more about my server, I hope to write about it soon. It's been a fun distraction/journey and I have a few learnings to share.


Usage
----

`python3 gol.py`

Curses will auto-detect terminal dimensions and run "full terminal". The bottom line is reserved for generation count and other stats.

Press `q` to Exit.

Press `r` to regenerate and start over.