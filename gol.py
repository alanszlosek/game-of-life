import curses
import datetime
import pickle
import random
import statistics
import time

# this will randomly populate up to 10 cells in each column
def cells(width, height, maxFlip=10):
    columnCells = [0] * height
    indices = range(height)
    firstGeneration = []
    a = []
    b = []
    x = 0
    while x < width:
        newColumnCells = columnCells.copy()
        # now flip some cells
        numberToFlip = random.randint(0, maxFlip)
        flipIndices = random.sample(indices, numberToFlip)
        for i in flipIndices:
            newColumnCells[i] = 1

        firstGeneration.append(
            newColumnCells.copy()
        )
        a.append(
            newColumnCells
        )
        b.append(
            columnCells.copy()
        )
        x += 1

    # add a glider
    #a[0][0] = 0
    #a[1][0] = 0
    #a[2][0] = 1
    #a[0][1] = 1
    #a[1][1] = 0
    #a[2][1] = 1
    #a[0][2] = 0
    #a[1][2] = 1
    #a[2][2] = 1
    return (firstGeneration, a,b)

def neighbors(cells, x, y, width, height):
    x1 = (x-1) % width
    x2 = (x+1) % width
    y1 = (y-1) % height
    y2 = (y+1) % height
    return (cells[x1][y1] +
        cells[x][y1] +
        cells[x2][y1] +
        cells[x1][y] +
        cells[x2][y] +
        cells[x1][y2] +
        cells[x][y2] +
        cells[x2][y2])


def main(stdscr):
    colors = [curses.COLOR_WHITE, curses.COLOR_RED, curses.COLOR_GREEN, curses.COLOR_YELLOW, curses.COLOR_BLUE, curses.COLOR_MAGENTA, curses.COLOR_CYAN]
    color_pair = random.randint(1, len(colors))
    i = 0
    while i < len(colors):
        # number, foreground, background
        # i+1 to skip 0, which is white on black
        curses.init_pair(i + 1, curses.COLOR_BLACK, colors[i])
        i += 1
    stdscr.nodelay(True)
    stdscr.clear()

    height, width = stdscr.getmaxyx()
    gridWidth = width
    gridHeight = height - 2
    # height-2 to leave a blank line between status and the GOL grid
    firstGeneration, genA, genB = cells(gridWidth, gridHeight)

    running = True
    generation = 0
    changeList = []
    while running:
        generation += 1
        changes = 0
        x = 0
        while x < gridWidth:
            y = 0
            while y < gridHeight:
                cell = genA[x][y]
                # draw current generation in ncurses cells
                if cell == 1:
                    stdscr.addch(y, x, ord(' '), curses.color_pair(color_pair))
                else:
                    stdscr.addch(y, x, ord(' '), curses.color_pair(0))

                newValue = 0
                numNeighbors = neighbors(genA, x, y, gridWidth, gridHeight)
                if cell == 1:
                    # Any live cell with fewer than two live neighbours dies, as if by underpopulation
                    # Any live cell with two or three live neighbours lives on to the next generation
                    # Any live cell with more than three live neighbours dies, as if by overpopulation.
                    if numNeighbors < 2:
                        newValue = 0
                        changes += 1
                    elif numNeighbors == 2 or numNeighbors == 3:
                        newValue = 1
                        changes += 1
                    elif numNeighbors > 3:
                        newValue = 0
                        changes += 1
                else:
                    # Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction
                    if numNeighbors == 3:
                        newValue = 1
                        changes += 1
                    else:
                        newValue = 0
                genB[x][y] = newValue
                
                y += 1
            x += 1

        # swap in new generation
        temp = genA
        genA = genB
        genB = temp
        
        stdscr.addstr(height - 1, 0, "generations: %d. changes this generation: %d.  Press q to exit. Press g to start a new generation        " % (generation, changes))
        # move cursor to bottom right
        stdscr.move(height - 1, width - 1)
        stdscr.refresh()

        time.sleep(0.25)
        k = stdscr.getch()
        if k == ord('q'):
            running = False
        if k == ord('g'):
            color_pair = random.randint(1, len(colors))
            firstGeneration, genA, genB = cells(gridWidth, gridHeight)
            generation = 0
            changeList = []
        
        changeList.append(changes)
        # 4 gens per second, so allow stagnation for 10 seconds
        stagnantGenerations = 4 * 10
        if len(changeList) >= stagnantGenerations:
            if len(changeList) > stagnantGenerations:
                changeList.pop(0)
            if statistics.mean(changeList) == changeList[0]:
                if generation > 1000:
                    # save the first generation that resulted in this long run
                    timestamp = datetime.datetime.today().strftime('%Y%m%d%H%M%S')
                    pickle.dump(firstGeneration, open('runs/%08d-%dx%d-%s.pickle' % (generation, gridWidth, gridHeight, timestamp), 'wb'))
                color_pair = random.randint(1, len(colors))
                firstGeneration, genA, genB = cells(gridWidth, gridHeight)
                generation = 0
                changeList = []

curses.wrapper(main)

#c = Cells(5, 5)
#c.cells[0][0] = 1
#c.cells[1][0] = 1
#c.cells[0][1] = 1
#c.cells[0][2] = 1
#print('neighbors: %d' % c.neighbors(4, 1))

# xxooo
# xoooo
